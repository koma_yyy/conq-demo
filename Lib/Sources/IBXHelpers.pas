unit IBXHelpers;

interface

uses
  Classes, IBDatabase, IBQuery;

type
  TIBQueryHelper = class helper for TIBQuery
  public
    procedure ReOpenWP(AParams: array of Variant); //FIBPlus like procedure...
    procedure FullRefresh; //FIBPlus like procedure...
    function GetCopy(AOwner: TComponent): TIBQuery;
  end;

implementation

{ TIBQueryHelper }

procedure TIBQueryHelper.FullRefresh;
begin
  if Active then Close;
  Open;
end;

function TIBQueryHelper.GetCopy(AOwner: TComponent): TIBQuery;
begin
  Result := TComponentClass(Self.ClassType).Create(AOwner);
  Result.Database := Self.Database;
  Result.Transaction := Self.Transaction;
  Result.DataSource := Self.DataSource;
  Result.SQL.Assign(Self.SQL);
  Result.Params.Assign(Self.Params);
  Result.Active := Self.Active;
end;

procedure TIBQueryHelper.ReOpenWP(AParams: array of Variant);
var
  I: Integer;
begin
  Close;
  if Length(AParams) = Params.Count then
    begin
      for I := Low(AParams) to High(AParams) do
        Params[I].Value := AParams[I];

      Open;
    end;
end;

end.
