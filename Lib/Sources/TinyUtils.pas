unit TinyUtils;

interface

uses
  SysUtils, Windows, Classes, Graphics, Dialogs;

type
  TStreamHelper = class helper for TStream
  public
    procedure Write(S: String); overload;
    procedure Write(S: String; Encoding: TEncoding); overload;
  end;

const
  mrYes = 6;

function ColorToHTML(Color: TColor): string;

function QuestionBox(Msg: string; HelpCtx: Integer = -1): Boolean; overload;
function QuestionBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1): Boolean; overload;
procedure InfoBox(Msg: string; HelpCtx: Integer = -1); overload;
procedure InfoBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1); overload;
procedure WarningBox(Msg: string; HelpCtx: Integer = -1); overload;
procedure WarningBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1); overload;
procedure ErrorBox(Msg: string; HelpCtx: Integer = -1); overload;
procedure ErrorBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1); overload;

implementation

{ TStreamHelper }

procedure TStreamHelper.Write(S: String);
begin
  Write(S, TEncoding.Default);
end;

procedure TStreamHelper.Write(S: String; Encoding: TEncoding);
var
  Bytes: TBytes;
begin
  Bytes := Encoding.GetBytes(S);
  Write(Bytes[0], Length(Bytes));
end;

function ColorToHTML(Color: TColor): string;
begin
  Result := '#' + IntToHex(GetRValue(Color), 2) +
    IntToHex(GetGValue(Color), 2) + IntToHex(GetBValue(Color), 2);
end;

function QuestionBox(Msg: string; HelpCtx: Integer = -1): Boolean;
begin
  Result := MessageDlg(Msg, mtConfirmation, mbYesNo, HelpCtx) = mrYes;
end;

function QuestionBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1): Boolean;
begin
  Result := QuestionBox(LoadResString(MsgIdent), HelpCtx);
end;

procedure InfoBox(Msg: string; HelpCtx: Integer = -1);
begin
  MessageDlg(Msg, mtInformation, [mbOK], HelpCtx);
end;

procedure InfoBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1);
begin
  InfoBox(LoadResString(MsgIdent), HelpCtx);
end;

procedure WarningBox(Msg: string; HelpCtx: Integer = -1);
begin
  MessageDlg(Msg, mtWarning, [mbOK], HelpCtx);
end;

procedure WarningBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1);
begin
  WarningBox(LoadResString(MsgIdent), HelpCtx);
end;

procedure ErrorBox(Msg: string; HelpCtx: Integer = -1);
begin
  MessageDlg(Msg, mtError, [mbOK], HelpCtx);
end;

procedure ErrorBox(MsgIdent: PResStringRec; HelpCtx: Integer = -1);
begin
  ErrorBox(LoadResString(MsgIdent), HelpCtx);
end;

end.
