unit uProgressForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls;

type
  TProgressForm = class(TForm)
    pbProgress: TProgressBar;
    btnAbort: TBitBtn;
    lblPrompt: TLabel;
    lblState: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnAbortClick(Sender: TObject);
  private
    FAbortCallback: TNotifyEvent;
    procedure SetState(const Value: string);
    procedure SetAbortCallback(const Value: TNotifyEvent);
  public
    procedure Show(Prompt: string); overload;
    procedure SetProgress(Position: Integer; Max: Integer);
    property State: string write SetState;
    property AbortCallback: TNotifyEvent read FAbortCallback write SetAbortCallback;
  end;

implementation

{$R *.dfm}

procedure TProgressForm.btnAbortClick(Sender: TObject);
begin
  Close;
end;

procedure TProgressForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
  if Assigned(FAbortCallback) then
    FAbortCallback(Self);
end;

procedure TProgressForm.SetAbortCallback(const Value: TNotifyEvent);
begin
  btnAbort.Visible := @Value <> nil;
  FAbortCallback := Value;
end;

procedure TProgressForm.SetProgress(Position, Max: Integer);
begin
  pbProgress.Position := Position;
  pbProgress.Max := Max;
  Application.ProcessMessages;
end;

procedure TProgressForm.SetState(const Value: string);
begin
  lblState.Caption := Value;
  Application.ProcessMessages;
end;

procedure TProgressForm.Show(Prompt: string);
begin
  lblPrompt.Caption := Prompt;

  inherited Show;
end;

end.
