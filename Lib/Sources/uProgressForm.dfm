object ProgressForm: TProgressForm
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'Progress'
  ClientHeight = 75
  ClientWidth = 384
  Color = clBtnFace
  Constraints.MaxHeight = 109
  Constraints.MinHeight = 109
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    384
    75)
  PixelsPerInch = 96
  TextHeight = 13
  object lblPrompt: TLabel
    Left = 8
    Top = 5
    Width = 287
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    ExplicitWidth = 325
  end
  object lblState: TLabel
    Left = 8
    Top = 55
    Width = 287
    Height = 13
    Alignment = taRightJustify
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    ExplicitWidth = 325
  end
  object pbProgress: TProgressBar
    Left = 8
    Top = 24
    Width = 287
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object btnAbort: TBitBtn
    Left = 301
    Top = 24
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Kind = bkAbort
    TabOrder = 1
    Visible = False
    OnClick = btnAbortClick
  end
end
