program conq_test;

uses
  Forms,
  HTMLExport in 'HTMLExport.pas',
  uEmployeesForm in 'uEmployeesForm.pas' {EmployeesForm},
  uDMDB in 'uDMDB.pas' {dmDB: TDataModule},
  uProgressForm in '.\..\Lib\Sources\uProgressForm.pas' {ProgressForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Test Task';
  Application.CreateForm(TdmDB, dmDB);
  Application.CreateForm(TEmployeesForm, EmployeesForm);
  Application.Run;
end.
