unit HTMLExport;

interface

uses
  Windows, SysUtils, Classes, Graphics, Forms, DB, IBQuery, IBXHelpers,
  SyncObjs;

resourcestring
  sBodyHeaderFmt = '<h2>Employees of <font color = "%s">%s</font> department</h2>';
  sBodyFooterFmt = '<BR><HR><p align="right">Generated at %s</p>';

  sFetchingData = 'Fetching data...';
  sReadyXofXFmt = 'Ready %d of %d';

  sThreadBusy = 'Thread is busy now!';
  sDocumentTitle = 'Department information';
  sDataSetNotActive = 'DataSet is not active';
  sNoVisibleColumns = 'No visible columns to export!';
  sDepartmentNameEmpty = 'Department name not specified';
  sDataSetNotSpecified = 'Dataset not specified';
  sOutputStramNotSpecified = 'Output stream not specified';

type
  EHTMLExport = class(Exception);

  TProgressEvent = procedure(Sender: TObject; const Current: Integer;
    const Total: Integer) of object;

  TStateEvent = procedure(Sender: TObject; State: string) of object;

  TErrorEvent = procedure(Sender: TObject; E: Exception) of object;

  THTMLExportThread = class(TThread)
  private const
    DelayInterval = 1800;  //delay for each record
  private
    DS: TIBQuery;
    FBusy: Boolean;
    FDepartmentName: string;
    FDataSet: TIBQuery;
    FOnProgress: TProgressEvent;
    FOnFinish: TNotifyEvent;
    FOnAbort: TNotifyEvent;
    FDepartmentColor: TColor;
    FOnState: TStateEvent;
    FOnError: TErrorEvent;
    FTotalPrc: Integer;
    FStatus: string;
    FCurrentPrc: Integer;
    FStream: TStream;
    procedure SetDataSet(const Value: TIBQuery);
    procedure CheckBusy;
    procedure SetDepartmentName(const Value: string);
    procedure SetDepartmentColor(const Value: TColor);
    procedure SetStream(const Value: TStream);
    function TD(Field: TField; IsTitle: Boolean; Width: Integer): string;
    function GetTableColumns(Fields: TFields): string;
  protected
    procedure Execute; override;
    procedure DoTerminate; override;
    procedure DoProgress(Current: Integer; Total: Integer); virtual;
    procedure DoStateChange(State: string); virtual;
    procedure DoFinish; virtual;
    procedure DoAbort; virtual;
    procedure DoError(E: Exception); virtual;
  public
    constructor Create(ADepartmentName: string; ADepartmentColor: TColor;
      ADataSet: TIBQuery); overload;
    property DepartmentName: string read FDepartmentName
      write SetDepartmentName;
    property DepartmentColor: TColor read FDepartmentColor
      write SetDepartmentColor;
    property DataSet: TIBQuery read FDataSet write SetDataSet;
    property Stream: TStream read FStream write SetStream;
    property Busy: Boolean read FBusy;
    property CurrentPrc: Integer read FCurrentPrc;
    property TotalPrc: Integer read FTotalPrc;
    property Status: string read FStatus;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
    property OnStateChange: TStateEvent read FOnState write FOnState;
    property OnFinish: TNotifyEvent read FOnFinish write FOnFinish;
    property OnAbort: TNotifyEvent read FOnAbort write FOnAbort;
    property OnError: TErrorEvent read FOnError write FOnError;
  end;

implementation

uses
  TinyUtils;

{ THTMLExportThread }

constructor THTMLExportThread.Create(ADepartmentName: string;
  ADepartmentColor: TColor; ADataSet: TIBQuery);
begin
  FDepartmentName := ADepartmentName;
  FDepartmentColor := ADepartmentColor;
  FDataSet := ADataSet;

  Create(True);
end;

procedure THTMLExportThread.DoAbort;
begin
  if Assigned(FOnAbort) then
    Synchronize(procedure
      begin
        FOnAbort(Self);
      end);
end;

procedure THTMLExportThread.DoError(E: Exception);
begin
  if Assigned(FOnError) then
    Synchronize(procedure
      begin
        FOnError(Self, E);
      end);
end;

procedure THTMLExportThread.DoFinish;
begin
  if Assigned(FOnFinish) then
    Synchronize(procedure
      begin
        FOnFinish(Self);
      end);
end;

procedure THTMLExportThread.DoProgress(Current, Total: Integer);
begin
  FCurrentPrc := Current;
  FTotalPrc := Total;
  if Assigned(FOnProgress) then
    Synchronize(procedure
      begin
        FOnProgress(Self, Current, Total);
      end);
end;

procedure THTMLExportThread.DoStateChange(State: string);
begin
  FStatus := State;
  if Assigned(FOnState) then
    Synchronize(procedure
      begin
        FOnState(Self, State);
      end);
end;

procedure THTMLExportThread.DoTerminate;
begin
  FreeAndNil(DS);

  //determining how process was terminated...
  if (FatalException <> nil) then
    begin //terminated by exception in thread
      FBusy := False;
      if FatalException is Exception then
        DoError(FatalException as Exception);
    end
  else if (FBusy) or (Terminated) then
    begin //aborted or terminated externally
      FBusy := False;
      DoAbort;
    end
  else
    DoFinish; //Work done succesfully!

  inherited; //Calling OnTerminate event
end;

procedure THTMLExportThread.CheckBusy;
begin
  if FBusy then
    raise EHTMLExport.CreateRes(@sThreadBusy);
end;

function THTMLExportThread.TD(Field: TField; IsTitle: Boolean;
  Width: Integer): string;
const
  StyleNames: array [0..2] of String = ('grid', 'gridr', 'gridc');
var
  S: String;
  Align: TAlignment;
  Tag: String;
begin
  if IsTitle then
    begin
      Tag := 'TH';
      Align := taLeftJustify;
      S := StyleNames[Integer(Align)];
    end
  else
    begin
      Tag := 'TD';
      Align := Field.Alignment;
      if Align = taLeftJustify then
        begin
          if (Field is TBCDField) or (Field is TCurrencyField) then
            Align := taRightJustify;
          if (Field is TBooleanField) then
            Align := taCenter;
        end;
      S := StyleNames[Integer(Align)];
      if (Field is TBCDField) or (Field is TIntegerField) then
        S := S + ' NOWRAP'
    end;
  if Width >  0 then
    S := S + Format(' WIDTH=" %d%%" ', [Width]);
  Result := '<' + Tag + ' class=' + S + '> ';
  if IsTitle then
    S := Field.DisplayName
  else
    begin
      if Field is TBooleanField then
        with TBooleanField(Field) do
          if Length(DisplayValues) = 0 then
            S := BoolToStr(True, True)
          else
            S := Field.DisplayText
      else
        S := Field.DisplayText;
    end;
  if Length(Trim(S)) = 0 then
    S := ' ';
  Result := Result + S + '</' + Tag + '> ' + sLineBreak;
end;

function THTMLExportThread.GetTableColumns(Fields: TFields): string;
var
  Widths: array of Integer;
  TotalWidth: Integer;

  procedure PrepareWidths;
  var
    I : Integer;
  begin
    TotalWidth := 0;
    Widths := nil;
    for I := 0 to Fields.Count - 1 do
      if Fields[I].Visible then
        begin
          SetLength(Widths, Length(Widths) + 1);
          Widths[I] := Fields[I].DisplayWidth;
          Inc(TotalWidth, Widths[I]);
        end;
    for I := Low(Widths) to High(Widths) do
      Widths[I] := Widths[I] * 100 div TotalWidth;
  end;
const
  HeaderRowStart = '<TR> ' + sLineBreak;
  HeaderRowEnd = '</TR> ' + sLineBreak;
var
  I: Integer;
begin
  PrepareWidths;
  Result := HeaderRowStart;
  for I := 0 to Fields.Count - 1 do
    if Fields[I].Visible then
      Result := Result + TD(Fields[I], True, Widths[I]);
  Result := Result + HeaderRowEnd;
end;

procedure THTMLExportThread.Execute;
  function GetHTMLHeader(Title: string): string;
  begin
    Result := '<HEAD><META http-equiv=Content-Type content=" text/html; charset=utf-8">' + sLineBreak +
     '<STYLE>' + sLineBreak +
     'BODY {' + sLineBreak +
     ' BACKGROUND: white;' + sLineBreak +
     ' COLOR: black;' + sLineBreak +
     ' FONT-FAMILY: arial;' + sLineBreak +
     ' FONT-SIZE: 8pt;' + sLineBreak +
     ' VERTICAL-ALIGN: top' + sLineBreak +
     '}' + sLineBreak +
     'TABLE {' + sLineBreak +
     ' BACKGROUND: white;' + sLineBreak +
     ' BORDER-BOTTOM: silver 0px solid;' + sLineBreak +
     ' BORDER-LEFT: silver 1px solid;' + sLineBreak +
     ' BORDER-RIGHT: silver 0px solid;' + sLineBreak +
     ' BORDER-TOP: silver 1px solid;' + sLineBreak +
     ' FONT-FAMILY: arial;' + sLineBreak +
     ' FONT-SIZE: 8pt;' + sLineBreak +
     ' FONT-WEIGHT: normal;' + sLineBreak +
     '}' + sLineBreak +
     'TD {' + sLineBreak +
     ' BORDER-BOTTOM: silver 1px solid;' + sLineBreak +
     ' BORDER-LEFT: silver 0px solid;' + sLineBreak +
     ' BORDER-RIGHT: silver 1px solid;' + sLineBreak +
     ' BORDER-TOP: silver 0px solid;' + sLineBreak +
     ' VERTICAL-ALIGN: top;' + sLineBreak +
     ' TEXT-ALIGN: left;' + sLineBreak +
     '}' + sLineBreak +
     'TD.grid {' + sLineBreak +
     ' TEXT-ALIGN: left;' + sLineBreak +
     '}' + sLineBreak +
     'TD.gridr {' + sLineBreak +
     ' TEXT-ALIGN: right;' + sLineBreak +
     '}' + sLineBreak +
     'TD.gridc {' + sLineBreak +
     ' TEXT-ALIGN: center;' + sLineBreak +
     '}' + sLineBreak +
     'TH {' + sLineBreak +
     ' BACKGROUND: silver;' + sLineBreak +
     ' BORDER-BOTTOM: gray 1px solid;' + sLineBreak +
     ' BORDER-LEFT: gray 0px solid;' + sLineBreak +
     ' BORDER-RIGHT: gray 1px solid;' + sLineBreak +
     ' BORDER-TOP: gray 0px solid;' + sLineBreak +
     ' FONT-WEIGHT: bold;' + sLineBreak +
     '}' + sLineBreak +
     'TH.grid {' + sLineBreak +
     ' TEXT-ALIGN: left;' + sLineBreak +
     '}' + sLineBreak +
     'TH.gridr {' + sLineBreak +
     ' TEXT-ALIGN: right;' + sLineBreak +
     '}' + sLineBreak +
     'TH.gridc {' + sLineBreak +
     ' TEXT-ALIGN: center;' + sLineBreak +
     '}' + sLineBreak +
     '</STYLE>' + sLineBreak +
     '<TITLE>' + Title + '</TITLE>' + sLineBreak +
     '</HEAD>' + sLineBreak;
  end;

const
  HTMLHead = '<!DOCTYPE HTML PUBLIC " -//W3C//DTD HTML 4.0 Transitional//EN">' + sLineBreak + '<HTML>' + sLineBreak;
  BodyStart = '<BODY>';
  BodyEnd = '</BODY>';
  HTMLEnd = '</HTML> ';
  TableStart = '<TABLE WIDTH=" 100%" CELLSPACING=0 CELLPADDING=1> ' + sLineBreak;
  TableEnd = '</TABLE>' + sLineBreak;
  BodyRowStart = '<TR>' + sLineBreak;
  BodyRowEnd = '</TR>' + sLineBreak;
var
  I: Integer;
  //_HTML, ColumnsHTML, BodyHeader, BodyFooter: string;
  ColumnsHTML: string;
  FieldsCount: Integer;
  StartPrc, CurrPos, TotalPrc, TotalRec: Integer;
begin
  if FDepartmentName = '' then
    raise EHTMLExport.CreateRes(@sDepartmentNameEmpty);
  if FDataSet = nil then
    raise EHTMLExport.CreateRes(@sDataSetNotSpecified);
  if FStream = nil then
    raise EHTMLExport.CreateRes(@sOutputStramNotSpecified);

  FBusy := True;
  FStream.Size := 0;

  Synchronize(procedure
    begin
      DS := FDataSet.GetCopy(nil); //making a copy of source dataset (SQL, Params and other required)
    end);

  if not DS.Active then
    raise EHTMLExport.CreateRes(@sDataSetNotActive);

  Synchronize(procedure
    begin
      FieldsCount := FDataSet.Fields.Count;
      ColumnsHTML := GetTableColumns(FDataSet.Fields);
    end);

  if FieldsCount = 0 then
    raise Exception.CreateRes(@sNoVisibleColumns);

  DoStateChange(sFetchingData);
  DoProgress(0, 0); //zero progress

  Sleep(DelayInterval * 2);
  if not DS.Bof then DS.First;
  DS.FetchAll;
  TotalRec := DS.RecordCount;
  StartPrc := TotalRec div 4; //(~25%)
  TotalPrc := TotalRec + StartPrc;
  CurrPos := 0;
  DoProgress(StartPrc, TotalPrc); //displaying progress on FetchAll operation - 25% of total

  Stream.Write(HTMLHead + GetHTMLHeader(Format(sDocumentTitle, [FDepartmentName]))
    + BodyStart + Format(sBodyHeaderFmt,
      [ColorToHTML(FDepartmentColor), FDepartmentName]) + TableStart + ColumnsHTML);

  while (not DS.Eof) and (not Terminated) do
    begin
      Inc(CurrPos);

      Stream.Write(BodyRowStart);
      for I := 0 to DS.Fields.Count - 1 do
        if DS.Fields[I].Visible then
          Stream.Write(TD(DS.Fields[I], False, -1));
      Stream.Write(BodyRowEnd);

      DoProgress(CurrPos + StartPrc, TotalPrc);
      DoStateChange(Format(sReadyXofXFmt, [CurrPos, TotalRec]));
      DS.Next;

      Sleep(DelayInterval);
    end;

  Stream.Write(TableEnd + (Format(sBodyFooterFmt,
    [DateTimeToStr(Now)]) + sLineBreak) + BodyEnd + HTMLEnd);

  FBusy := False;
end;

procedure THTMLExportThread.SetDataSet(const Value: TIBQuery);
begin
  CheckBusy;
  FDataSet := Value
end;

procedure THTMLExportThread.SetDepartmentColor(const Value: TColor);
begin
  CheckBusy;
  FDepartmentColor := Value;
end;

procedure THTMLExportThread.SetDepartmentName(const Value: string);
begin
  CheckBusy;
  FDepartmentName := Value;
end;

procedure THTMLExportThread.SetStream(const Value: TStream);
begin
  CheckBusy;
  FStream := Value;
end;

end.
