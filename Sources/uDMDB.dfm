object dmDB: TdmDB
  OldCreateOrder = False
  Height = 321
  Width = 539
  object ibdb: TIBDatabase
    Params.Strings = (
      'user_name=sysdba'
      'password=masterke')
    DefaultTransaction = ibtr
    Left = 24
    Top = 8
  end
  object ibtr: TIBTransaction
    DefaultDatabase = ibdb
    Left = 56
    Top = 40
  end
  object dsEmployees: TIBQuery
    Database = ibdb
    Transaction = ibtr
    SQL.Strings = (
      'select'
      '    e.first_name,'
      '    e.last_name,'
      '    e.email,'
      '    e.phone_number,'
      '    e.hire_date,'
      '    e.job_id,'
      '    e.salary,'
      '    e.commission_pct,'
      '    m.first_name || '#39' '#39' || m.last_name as manager_name'
      'from employees e'
      'join employees m on m.employee_id = e.manager_id'
      'where e.department_id = :department_id'
      'order by first_name, last_name asc')
    Left = 140
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'department_id'
        ParamType = ptUnknown
      end>
    object fld_EmployeesFIRST_NAME: TIBStringField
      DisplayLabel = 'First Name'
      DisplayWidth = 20
      FieldName = 'FIRST_NAME'
      Origin = '"EMPLOYEES"."FIRST_NAME"'
      Required = True
      Size = 80
    end
    object fld_EmployeesLAST_NAME: TIBStringField
      DisplayLabel = 'Last Name'
      DisplayWidth = 25
      FieldName = 'LAST_NAME'
      Origin = '"EMPLOYEES"."LAST_NAME"'
      Required = True
      Size = 100
    end
    object fld_EmployeesEMAIL: TIBStringField
      DisplayLabel = 'E-Mail'
      DisplayWidth = 25
      FieldName = 'EMAIL'
      Origin = '"EMPLOYEES"."EMAIL"'
      Required = True
      Size = 100
    end
    object fld_EmployeesPHONE_NUMBER: TIBStringField
      DisplayLabel = 'Phone number'
      DisplayWidth = 20
      FieldName = 'PHONE_NUMBER'
      Origin = '"EMPLOYEES"."PHONE_NUMBER"'
      Size = 80
    end
    object dtfldEmployeesHIRE_DATE: TDateField
      DisplayLabel = 'Hire date'
      FieldName = 'HIRE_DATE'
      Origin = '"EMPLOYEES"."HIRE_DATE"'
      Required = True
    end
    object fld_EmployeesJOB_ID: TIBStringField
      DisplayLabel = 'Job Ident'
      DisplayWidth = 10
      FieldName = 'JOB_ID'
      Origin = '"EMPLOYEES"."JOB_ID"'
      Required = True
      Size = 40
    end
    object fld_EmployeesSALARY: TIBBCDField
      DisplayLabel = 'Salary'
      DisplayWidth = 11
      FieldName = 'SALARY'
      Origin = '"EMPLOYEES"."SALARY"'
      Required = True
      Precision = 18
      Size = 2
    end
    object fld_EmployeesCOMMISSION_PCT: TIBBCDField
      DisplayLabel = 'Commission (%)'
      DisplayWidth = 15
      FieldName = 'COMMISSION_PCT'
      Origin = '"EMPLOYEES"."COMMISSION_PCT"'
      Precision = 4
      Size = 2
    end
    object fld_EmployeesMANAGER_NAME: TIBStringField
      DisplayLabel = 'Manager Name'
      DisplayWidth = 45
      FieldName = 'MANAGER_NAME'
      ProviderFlags = []
      Size = 184
    end
  end
  object dssEmployees: TDataSource
    AutoEdit = False
    DataSet = dsEmployees
    Left = 152
    Top = 152
  end
  object dsDepartments: TIBQuery
    Database = ibdb
    Transaction = ibtr
    SQL.Strings = (
      'select'
      '    d.department_id,'
      '    d.department_name,'
      
        '    (select sum(e.salary) from employees e where e.department_id' +
        ' = d.department_id) as salary_sum'
      'from departments d'
      'order by salary_sum desc')
    Left = 92
    Top = 88
    object fld_DepartmentsDEPARTMENT_ID: TIntegerField
      FieldName = 'DEPARTMENT_ID'
      Origin = '"DEPARTMENTS"."DEPARTMENT_ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object fld_DepartmentsDEPARTMENT_NAME: TIBStringField
      FieldName = 'DEPARTMENT_NAME'
      Origin = '"DEPARTMENTS"."DEPARTMENT_NAME"'
      Required = True
      Size = 120
    end
    object fld_DepartmentsSALARY_SUM: TIBBCDField
      FieldName = 'SALARY_SUM'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
  end
end
