unit uDMDB;

interface

uses
  Windows, SysUtils, Classes, IBDatabase, DB, IBSQL, IBCustomDataSet, IBQuery,
  Generics.Collections, Variants, IBXHelpers;

type
  TDepartment = record
    ID: Integer;
    Name: string;
    Salary: Currency;
  end;

  TDeptList = class(TList<TDepartment>)
  private var
    Dept: TDepartment;
  public
    procedure Add(const ID: Integer; const Name: string; const Salary: Currency);
  end;

  TdmDB = class(TDataModule)
    ibdb: TIBDatabase;
    ibtr: TIBTransaction;
    dsEmployees: TIBQuery;
    fld_EmployeesFIRST_NAME: TIBStringField;
    fld_EmployeesLAST_NAME: TIBStringField;
    fld_EmployeesEMAIL: TIBStringField;
    fld_EmployeesPHONE_NUMBER: TIBStringField;
    dtfldEmployeesHIRE_DATE: TDateField;
    fld_EmployeesJOB_ID: TIBStringField;
    fld_EmployeesSALARY: TIBBCDField;
    fld_EmployeesCOMMISSION_PCT: TIBBCDField;
    fld_EmployeesMANAGER_NAME: TIBStringField;
    dssEmployees: TDataSource;
    dsDepartments: TIBQuery;
    fld_DepartmentsDEPARTMENT_ID: TIntegerField;
    fld_DepartmentsDEPARTMENT_NAME: TIBStringField;
    fld_DepartmentsSALARY_SUM: TIBBCDField;
  private
    FDeptList: TDeptList;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ScrollDept(DeptID: Variant);
    procedure RefreshContext(ConnectionString: string);
    property DeptList: TDeptList read FDeptList;
  end;

var
  dmDB: TdmDB;

implementation

{$R *.dfm}

constructor TdmDB.Create(AOwner: TComponent);
begin
  inherited;

  FDeptList := TDeptList.Create;
end;

destructor TdmDB.Destroy;
begin
  FDeptList.Free;

  inherited;
end;

procedure TdmDB.ScrollDept(DeptID: Variant);
begin
  dsEmployees.ReOpenWP([DeptID])
end;

procedure TdmDB.RefreshContext(ConnectionString: string);
begin
  try
    ibdb.Connected := False;
    ibdb.DatabaseName := ConnectionString;
    ibdb.Connected := True;

    FDeptList.Clear;

    dsDepartments.FullRefresh;
    while not dsDepartments.Eof do
      begin
        FDeptList.Add(fld_DepartmentsDEPARTMENT_ID.Value,
          fld_DepartmentsDEPARTMENT_NAME.Value,
          fld_DepartmentsSALARY_SUM.Value);

        dsDepartments.Next;
      end;
    FDeptList.TrimExcess;
  except
    FDeptList.Clear;
    raise;
  end;
end;

{ TDeptList }

procedure TDeptList.Add(const ID: Integer; const Name: string;
  const Salary: Currency);
begin
  Dept.ID := ID;
  Dept.Name := Name;
  Dept.Salary := Salary;

  inherited Add(Dept);
end;

var
  CurrDir: string;

initialization
begin
  CurrDir := GetCurrentDir + '\';
  if FileExists(CurrDir + 'gds32.dll') then
    SetEnvironmentVariable('FIREBIRD', PChar(CurrDir));
end;

end.
