unit uEmployeesForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, TinyUtils,
  ShellAPI, uProgressForm;

resourcestring
  sAbortConfirm = 'Are you sure you want to abort this operation?';
  sProgressPromptFmt = '%s Department Employees';
  sProgressCaption = 'Exporting progress';
  sExportAbortedFmt = 'Export of %s aborted!';
  sOpenResultsConfirmFmt = 'Department %s exported!' + sLineBreak + sLineBreak +
      'Do you want open results?';

const
  BlueSalary = 50000; //Blue dept color starts from this value

  DeptColorBlue = clBlue;
  DeptColorRed =  clRed;

type
  TEmployeesForm = class(TForm)
    gbDept: TGroupBox;
    gbEmpl: TGroupBox;
    pnlCtrl: TPanel;
    btnExportHTML: TBitBtn;
    lvDept: TListView;
    pnlEmpl: TPanel;
    dbgrdEmployees: TDBGrid;
    btnConnect: TBitBtn;
    dlgOpenDBSelect: TOpenDialog;
    spl1: TSplitter;
    procedure lvDeptCustomDrawItem(Sender: TCustomListView; Item: TListItem;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure lvDeptChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure btnConnectClick(Sender: TObject);
    procedure btnExportHTMLClick(Sender: TObject);
  private
    function GetColorBySalary(Salary: Currency; DefaultColor: TColor): TColor;
    function FindProgress(ThreadObject: TObject): TProgressForm;
    procedure LoadDepartments;
    procedure AbortClick(Sender: TObject);
    //events from thread
    procedure HTMLExportState(Sender: TObject; State: string);
    procedure HTMLExportProgress(Sender: TObject; const Current: Integer;
      const Total: Integer);
    procedure HTMLExportFinish(Sender: TObject);
    procedure HTMLExportAborted(Sender: TObject);
    procedure HTMLExportError(Sender: TObject; E: Exception);
    procedure HTMLExportTerminated(Sender: TObject);
  end;

var
  EmployeesForm: TEmployeesForm;

implementation

uses
  uDMDB, HTMLExport;

{$R *.dfm}

procedure TEmployeesForm.AbortClick(Sender: TObject);
begin
  if (Sender is TProgressForm)
      and (TObject((Sender as TProgressForm).Tag) is TThread)
      and (QuestionBox(@sAbortConfirm)) then
    (TObject((Sender as TProgressForm).Tag) as TThread).Terminate;
end;

procedure TEmployeesForm.btnConnectClick(Sender: TObject);
begin
  if dlgOpenDBSelect.Execute then
    begin
      try
        dmDB.RefreshContext(dlgOpenDBSelect.FileName);
      finally
        LoadDepartments;
      end;
    end;
end;

procedure TEmployeesForm.btnExportHTMLClick(Sender: TObject);
var
  Dept: TDepartment;
  T: THTMLExportThread;
begin
  if lvDept.ItemIndex < 0 then Exit;

  Dept := dmDB.DeptList[Integer(lvDept.Items[lvDept.ItemIndex].Data)];

  T := THTMLExportThread.Create(Dept.Name, //dept name
        GetColorBySalary(Dept.Salary, dbgrdEmployees.Font.Color), //dept color in LV
        dmDB.dsEmployees);//dataset to export
  with T do
    begin
      Stream := TMemoryStream.Create;
      with TProgressForm.Create(Self) do
        begin
          Tag := Integer(T);
          Caption := sProgressCaption;
          AbortCallback := AbortClick;
          Show(Format(sProgressPromptFmt, [Dept.Name]));
        end;
      OnFinish := HTMLExportFinish;
      OnAbort := HTMLExportAborted;
      OnProgress := HTMLExportProgress;
      OnStateChange := HTMLExportState;
      OnError := HTMLExportError;
      OnTerminate := HTMLExportTerminated;
      FreeOnTerminate := True;
      Start;
    end;
end;

function TEmployeesForm.FindProgress(ThreadObject: TObject): TProgressForm;
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
    if (Components[I] is TProgressForm)
        and (TObject((Components[I] as TProgressForm).Tag) = ThreadObject) then
          Exit(Components[I] as TProgressForm);

  Result := nil;
end;

function TEmployeesForm.GetColorBySalary(Salary: Currency;
  DefaultColor: TColor): TColor;
begin
  if Salary = 0 then
    Result := DeptColorRed
  else if Salary > BlueSalary then
    Result := DeptColorBlue
  else
    Result := DefaultColor;
end;

procedure TEmployeesForm.LoadDepartments;
var
  I: Integer;
begin
  lvDept.Clear;
  for I := 0 to dmDB.DeptList.Count - 1 do
    lvDept.AddItem(dmDB.DeptList[I].Name, TObject(I));

  pnlEmpl.Visible := lvDept.Items.Count > 0;
end;

procedure TEmployeesForm.lvDeptChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  dbgrdEmployees.Visible := lvDept.ItemIndex > -1;

  if lvDept.ItemIndex > -1 then
    dmDB.ScrollDept(dmDB.DeptList[Integer(lvDept.Items[lvDept.ItemIndex].Data)].ID);

  btnExportHTML.Enabled := (lvDept.ItemIndex > -1)
    and (not dmDB.dsEmployees.IsEmpty);
end;

procedure TEmployeesForm.lvDeptCustomDrawItem(Sender: TCustomListView;
  Item: TListItem; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  Sender.Canvas.Font.Color :=
    GetColorBySalary(dmDB.DeptList[Integer(Item.Data)].Salary,
      Sender.Canvas.Font.Color);
end;

procedure TEmployeesForm.HTMLExportAborted(Sender: TObject);
begin
  WarningBox(Format(sExportAbortedFmt,
    [(Sender as THTMLExportThread).DepartmentName]));
end;

procedure TEmployeesForm.HTMLExportError(Sender: TObject; E: Exception);
begin
  ErrorBox(Format('Exception ' + '''%s''' + 'raised!' + sLineBreak +
    sLineBreak + '%s', [E.ClassName, E.Message]));
end;

procedure TEmployeesForm.HTMLExportFinish(Sender: TObject);
var
  FileName, DeptName: string;
begin
  with TMemoryStream((Sender as THTMLExportThread).Stream) do
    begin
      DeptName := (Sender as THTMLExportThread).DepartmentName;
      FileName := DeptName + '.html';
      SaveToFile(FileName);
    end;
  if QuestionBox(Format(sOpenResultsConfirmFmt, [DeptName])) then
    ShellExecute(0, '', PChar(FileName), '', '', SW_SHOWNORMAL);
end;

procedure TEmployeesForm.HTMLExportProgress(Sender: TObject; const Current,
  Total: Integer);
var
  ProgressForm: TProgressForm;
begin
  //find form and set progress
  ProgressForm := FindProgress(Sender);
  if ProgressForm <> nil then
    ProgressForm.SetProgress(Current, Total);
end;

procedure TEmployeesForm.HTMLExportState(Sender: TObject; State: string);
var
  ProgressForm: TProgressForm;
begin
  //find form and refresh status
  ProgressForm := FindProgress(Sender);
  if ProgressForm <> nil then
    ProgressForm.State := State;
end;

procedure TEmployeesForm.HTMLExportTerminated(Sender: TObject);
var
  ProgressForm: TProgressForm;
begin
  //find form and close window
  ProgressForm := FindProgress(Sender);
  if ProgressForm <> nil then
    ProgressForm.Free;
  (Sender as THTMLExportThread).Stream.Free; //realising the stream associated with thread
  (Sender as THTMLExportThread).Stream := nil;
end;

end.
