object EmployeesForm: TEmployeesForm
  Left = 0
  Top = 0
  ActiveControl = btnConnect
  Caption = 'Departments&Employees'
  ClientHeight = 390
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object spl1: TSplitter
    Left = 185
    Top = 0
    Height = 349
    ExplicitLeft = 112
    ExplicitTop = 72
    ExplicitHeight = 100
  end
  object gbDept: TGroupBox
    Left = 0
    Top = 0
    Width = 185
    Height = 349
    Align = alLeft
    Caption = 'Departments'
    TabOrder = 0
    object lvDept: TListView
      Left = 2
      Top = 15
      Width = 181
      Height = 332
      Align = alClient
      Columns = <>
      ReadOnly = True
      ShowColumnHeaders = False
      TabOrder = 0
      ViewStyle = vsList
      OnChange = lvDeptChange
      OnCustomDrawItem = lvDeptCustomDrawItem
    end
  end
  object gbEmpl: TGroupBox
    Left = 188
    Top = 0
    Width = 536
    Height = 349
    Align = alClient
    Caption = 'Employees'
    TabOrder = 1
    object pnlEmpl: TPanel
      Left = 2
      Top = 15
      Width = 532
      Height = 332
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Select department for view employees list'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Visible = False
      object dbgrdEmployees: TDBGrid
        Left = 0
        Top = 0
        Width = 532
        Height = 332
        Align = alClient
        DataSource = dmDB.dssEmployees
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
  end
  object pnlCtrl: TPanel
    Left = 0
    Top = 349
    Width = 724
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      724
      41)
    object btnExportHTML: TBitBtn
      Left = 591
      Top = 6
      Width = 123
      Height = 25
      Hint = 'Export employees data to HTML format'
      Anchors = [akTop, akRight]
      Caption = '-> HTML'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnExportHTMLClick
    end
    object btnConnect: TBitBtn
      Left = 10
      Top = 6
      Width = 123
      Height = 25
      Hint = 'Export employees data to HTML format'
      Caption = 'Connect/Refresh'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnConnectClick
    end
  end
  object dlgOpenDBSelect: TOpenDialog
    Filter = 
      'Database|data_db.fdb|All FB/IB Databases|*.fdb;*.gdb|All Files|*' +
      '.*'
    Title = 'DB Connection string'
    Left = 72
    Top = 104
  end
end
